package Project1;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;

public class SimpleModel extends AbstractTableModel {
    TableModel tableModel = new TableModel("hi", "video", "ugugg", 1);
    ArrayList<TableModel>  arrayList=new ArrayList<>();


    @Override
    public int getRowCount() {
        return arrayList.size();
    }

    public int getColumnCount() {
        return 5;

    }

    public Class<?> getColumnClass(int column) {
        switch (column) {
            case 0:
                return Integer.class;
            case 1:
            case 2:
            case 3:
                return String.class;
            default:
                return Object.class;
        }
    }

    public Object getValueAt(int row, int column) {
        switch (column) {
            case 0:
                return tableModel.getId();
            case 1:
                return tableModel.getName();
            case 2:
                return tableModel.getType();
            case 3:
                return tableModel.getPath();
        }
        return "Не определена";
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 0:
                return "Number";
            case 1:
                return "Name";
            case 2:
                return "Type";
            case 3:
                return "Path";
        }
        return "No init";
    }
}
