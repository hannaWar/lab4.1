package Project1;

import java.util.ArrayList;

public class TableModel {
    private String name;
    private String type;
    private String path;
    private int id;

    TableModel(String name,String type,String path,int id){
        this.id=id;
        this.name=name;
        this.path=path;
        this.type=type;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPath() {
        return path;
    }

    public String getType() {
        return type;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public void setType(String type) {
        this.type = type;
    }
}

