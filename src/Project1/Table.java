package Project1;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.io.IOException;

public class Table extends JFrame {
    User user;
    private JTable table;
    private boolean isEnabled = true;
    private int temp;

    public Table() {

        JTable table = new JTable(new SimpleModel());
        table.setRowHeight(24);
        Box contents = new Box(BoxLayout.Y_AXIS);
        contents.add(new JScrollPane(table));
        getContentPane().add(contents);
//        table.setCellSelectionEnabled(true);
        table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent listSelectionEvent) {
                if (!isEnabled) {
                    isEnabled = true;
                    return;
                }
                isEnabled = false;
                if (table.getSelectedColumn() == 2) {
                    final var path = table.getModel().getValueAt(table.getSelectedRow(), table.getSelectedColumn()).toString();
                    try {
                        Runtime.getRuntime().exec("dolphin --select +" + path);//выполнение команд ОС
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(400, 300);
        setVisible(true);
    }
}
