import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;


public class GraphicPanel extends JPanel {
    private boolean temp;
    private double x;
    private double y;
    GraphicPanel() {
        super();
        this.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                x = e.getX();
                System.out.println(x);
                y = e.getY();
                System.out.println(y);
                temp = s1.contains(x , y) || s2.contains(x , y);
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });
    }

    public void paint(Graphics graphics) {
        super.paint(graphics);
        Graphics2D graphics2D = (Graphics2D) graphics;
        graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        drawGrid(graphics2D);//сетка
        drawAxis(graphics2D);//оси
        drawGraph(graphics2D);//график
    }

    private Shape s1;
    private Shape s2;

    private void drawGraph(Graphics2D graphics2D) {
        final int height = getHeight();
        final int width = getWidth();
        Color color = graphics2D.getColor();
        final int cy = height / 2;
        final int cx = width / 2;
        graphics2D.setColor(Color.blue);
        graphics2D.setStroke(new BasicStroke(3.0f));
        s1 = new Rectangle(cx - 120, cy + 10, 240, 60);
        graphics2D.fill(s1);
        s2 = new Rectangle(cx - 80, cy - 90, 160, 100);
        graphics2D.fill(s2);
        graphics2D.setColor(color);
        graphics2D.setFont(Font.getFont(Font.SANS_SERIF));
        if (temp) {
            graphics2D.drawString("TRUE", 600, 600);
        } else {
            graphics2D.drawString("FALSE", 600, 600);
        }
        this.repaint();
    }

    //рисование сетки
    private void drawGrid(Graphics2D graphics2D) {
        final int d = 20;
        final int height = getHeight();
        final int width = getWidth();
        final int cy = 10 + height / 2;
        final int cx = width / 2;
        Color c = graphics2D.getColor();
        graphics2D.setColor(Color.gray);
        for (int x = cx % d; x < width; x += d) {
            graphics2D.drawLine(x, 0, x, height);
        }
        for (int y = cy % d; y < height; y += d) {
            graphics2D.drawLine(0, y, width, y);
        }
        graphics2D.setColor(c);
    }

    //Рисование осей координат
    private void drawAxis(Graphics2D g2) {
        Color c = g2.getColor();
        final int width = getWidth();
        final int height = getHeight();
        g2.setStroke(new BasicStroke(2.0f));
        g2.setColor(Color.black);
        g2.drawLine(0, 10 + height / 2, width, 10 + height / 2);
        g2.drawLine(width / 2, 0, width / 2, height);
        g2.setColor(c);
    }
}

