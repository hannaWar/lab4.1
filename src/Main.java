import Project1.FirstDialog;

import java.util.Scanner;

public class Main {
    static void result(double x, double y) {
        try {
            double result = ((1 + Math.pow(Math.sin(x + y), 2)) / 2 + Math.abs(x - (2 * x / (1 + Math.pow(x, 2) * Math.pow(y, 2))))) + x;
            System.out.println(result);
        } catch (ArithmeticException e) {
            System.out.println("Error" + e.toString());
        }
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int num = sc.nextInt();
        switch (num) {
            case 1:
                System.out.println("Enter first value:");
                double x = sc.nextDouble();
                System.out.println("Enter second value:");
                double y = sc.nextDouble();
                Main.result(x, y);
                break;
            case 2:
                new Example1().setVisible(true);
                break;
            case 3:
                new FirstDialog();

                break;
            case 4:
                double a = 1;
                double b = 2;
                new SimpleTable3(a, b);
                break;

        }
    }
}
