import javax.swing.table.AbstractTableModel;

public class SimpleModel3 extends AbstractTableModel {
    private double a;
    private double p;

    public SimpleModel3(double a, double p) {
        this.a = a;
        this.p = p;
    }

    public int getRowCount() {
        return 10;
    }

    public int getColumnCount() {
        return 2;
    }

    public Class<?> getColumnClass(int column) {
        switch (column) {
            case 0:
            case 1:
                return Double.class;
            default:
                return Object.class;
        }
    }


    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        var t  = a + rowIndex * p;
        switch (columnIndex) {
            case 0:
                return t;
            case 1:
                return Math.tan(t);
            default:
                return "NO";
        }
    }
    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 0:
                return "x";
            case 1:
                return "F(x)";
            default:
                return "Не определена";
        }
    }
}
