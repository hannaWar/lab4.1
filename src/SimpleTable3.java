import javax.swing.*;

public class SimpleTable3 extends JFrame {
    private JTable table1;
    private JPanel panel1;

    public SimpleTable3(double a, double b) {
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        JTable table = new JTable(new SimpleModel3(a, (b - a) / 9));
        table.setRowHeight(24);
        Box contents = new Box(BoxLayout.Y_AXIS);
        contents.add(new JScrollPane(table));
        getContentPane().add(contents);
        setSize(400, 300);
        setVisible(true);

    }
}
